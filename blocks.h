//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/	/*Command*/		                        /*Update Interval*/	/*Update Signal*/
	{" ", "free -h | awk '/^Mem/ { print $3\" / \"$2 }' | sed s/i//g",	10,		0},
	{" ", "awk '{print $0\"%\"}' /sys/class/power_supply/BAT?/capacity", 10, 0},
	{" ", "date +%R",            1,    0},
	{"糖 ", "sensors | awk '/Core 0/ { print $3 }' | sed 's/+//;s/\\.[0-9]//' ", 10, 0},
	{"蓼 ", "amixer sget Master|grep %|awk -F'[][]' '{ print $2 }' ", 0, 10},
	{" ", "xbacklight", 0, 15},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "   ";
static unsigned int delimLen = 5;
