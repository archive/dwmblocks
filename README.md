# dwmblocks
Modular status bar for dwm written in c.

# my version
I made my own version of dwmblocks to be used with [my build of dwm](https://gitlab.com/vitali64/dots/v64-dwm)
# usage
To use dwmblocks first run 'make' and then install it with 'sudo make install'.
After that you can put dwmblocks in your xinitrc or other startup script to have it start with dwm.
# modifying blocks
The statusbar is made from text output from commandline programs.
Blocks are added and removed by editing the blocks.h header file.
By default the blocks.h header file is created the first time you run make which copies the default config from blocks.def.h.
This is so you can edit your status bar commands and they will not get overwritten in a future update.
